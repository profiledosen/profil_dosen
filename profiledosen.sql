-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Nov 2016 pada 15.38
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `profiledosen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
`id` int(11) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `isi_berita` varchar(2000) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id`, `judul`, `gambar`, `isi_berita`, `tanggal`, `id_admin`) VALUES
(1, 'Wisuda UIN Sunan Gunung Djati Bandung', 'iphone-5-home-screen-ios-7.jpg', '<p>Wisuda UIN SGD Bandung 2016</p>\r\n', '2016-11-21', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE IF NOT EXISTS `buku` (
`id` int(11) NOT NULL,
  `judul_buku` varchar(200) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `isbn` varchar(50) DEFAULT NULL,
  `penerbit` varchar(50) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pendidikan`
--

CREATE TABLE IF NOT EXISTS `detail_pendidikan` (
`id` int(11) NOT NULL,
  `id_jenjang` int(11) DEFAULT NULL,
  `nama_instansi` varchar(50) DEFAULT NULL,
  `thn_masuk` int(11) DEFAULT NULL,
  `thn_keluar` int(11) DEFAULT NULL,
  `program_studi` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE IF NOT EXISTS `dosen` (
`id` int(11) NOT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `gelar` varchar(50) DEFAULT NULL,
  `kode_jurusan` varchar(10) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`id`, `nip`, `nama`, `gelar`, `kode_jurusan`, `username`, `password`) VALUES
(1, '11111', 'aasdf', 'qw', '705', 'dosen', '81dc9bdb52d04dc20036dbd8313ed055'),
(3, '123123', 'Copocol', 'MT', '705', NULL, NULL),
(4, '123123123', 'Ridha Shabrina', 'MT', '705', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fak`
--

CREATE TABLE IF NOT EXISTS `fak` (
`kode_fak` int(11) NOT NULL,
  `fakultas` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `fak`
--

INSERT INTO `fak` (`kode_fak`, `fakultas`) VALUES
(1, 'USHULUDIN'),
(2, 'SAIN DAN TEKNOLOGI'),
(3, 'TARBIYAH DAN KEGURUAN'),
(4, 'ADAB DAN HUMANIORA'),
(5, 'DAKWAH DAN KOMUNIKASI'),
(6, 'PSIKOLOGI'),
(7, 'FISIP'),
(8, 'SYARIAH DAN HUKUM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
`id` int(11) NOT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `institusi` varchar(200) DEFAULT NULL,
  `tahun` varchar(50) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenjang_pendidikan`
--

CREATE TABLE IF NOT EXISTS `jenjang_pendidikan` (
`id` int(11) NOT NULL,
  `nama_jenjang` varchar(50) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `jenjang_pendidikan`
--

INSERT INTO `jenjang_pendidikan` (`id`, `nama_jenjang`, `id_dosen`) VALUES
(1, 'SD', NULL),
(2, 'SMP', NULL),
(3, 'MTS', NULL),
(4, 'MI', NULL),
(5, 'SMA', NULL),
(6, 'MA', NULL),
(7, 'SMK', NULL),
(8, 'S1', NULL),
(9, 'S2', NULL),
(10, 'S3', NULL),
(11, 'D1', NULL),
(12, 'D3', NULL),
(13, 'D4', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurnal`
--

CREATE TABLE IF NOT EXISTS `jurnal` (
`id` int(11) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `nama_jurnal` varchar(100) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `volume` varchar(50) DEFAULT NULL,
  `no_registrasi` varchar(100) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
  `kode_jurusan` varchar(10) NOT NULL,
  `kode_fak` int(11) NOT NULL,
  `nama_jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`kode_jurusan`, `kode_fak`, `nama_jurusan`) VALUES
('101', 1, 'Aqidah dan Filsafat'),
('102', 1, 'Perbandingan Agama'),
('103', 1, 'Tafsir Hadist'),
('104', 1, 'Tasawuf Psikoterapi'),
('201', 3, 'Manajemen Pendidikan Islam'),
('202', 3, 'Pendidikan Agama Islam'),
('203', 3, 'Pendidikan Bahasa Arab'),
('204', 3, 'Pendidikan Bahasa Inggris'),
('205', 3, 'Pendidikan Matematika'),
('206', 3, 'Pendidikan Biologi'),
('207', 3, 'Pendidikan Fisika'),
('208', 3, 'Pendidikan Kimia'),
('209', 3, 'Pendidikan Guru MI'),
('301', 8, 'Ahwal Al Syakhsiyah'),
('302', 8, 'Muamalah'),
('303', 8, 'Siyasah'),
('304', 8, 'Perbandingan Madzhab dan Hukum'),
('305', 8, 'Ilmu Hukum'),
('306', 8, 'Hukum Pidana Islam'),
('307', 8, 'Manajemen Keuangan Syariah'),
('401', 5, 'Bimbingan Konseling Islam'),
('402', 5, 'Komunikasi Penyiaran Islam'),
('403', 5, 'Manajemen Dakwah'),
('404', 5, 'Pengantar Masyarakan Islam'),
('405', 5, 'Ilmu Komunikasi Jurnalistik'),
('406', 5, 'Ilmu Komunikasi Humas'),
('501', 4, 'Sejarah Peradaban Islam'),
('502', 4, 'Bahasa dan Sastra Arab'),
('503', 4, 'Bahasa dan Sastra Inggris'),
('505', 4, 'D-3 Terjemah Bahasa Inggris'),
('600', 6, 'Psikologi'),
('701', 2, 'Matematika'),
('702', 2, 'Biologi'),
('703', 2, 'Fisika'),
('704', 2, 'Kimia'),
('705', 2, 'Teknik Informatika'),
('706', 2, 'Agroteknologi'),
('707', 2, 'Teknik Elektro'),
('801', 7, 'Administrasi Negara'),
('802', 7, 'Manajemen'),
('803', 7, 'Sosiologi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karya_ilmiah`
--

CREATE TABLE IF NOT EXISTS `karya_ilmiah` (
`id` int(11) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `karya_ilmiah`
--

INSERT INTO `karya_ilmiah` (`id`, `judul`, `tahun`, `jenis`, `id_kategori`, `id_dosen`) VALUES
(1, 'asasa', 12312, 'asada', NULL, NULL),
(2, 'asa', 1231, 'asad', 1, NULL),
(3, 'asdasadasa', 12312, 'ahfsahjas', 1, NULL),
(5, 'dada', 1231, 'adas', NULL, 3),
(6, 'dasd', 1231, 'adas', NULL, NULL),
(7, 'fa', 3333, '123', 1, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id` int(11) NOT NULL,
  `nama_kategori` varchar(50) DEFAULT NULL,
  `id_tabel` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `id_tabel`) VALUES
(1, 'Nasional', 4),
(2, 'ISSN', 3),
(3, 'Unggulan', 4),
(4, 'Lokal', 1),
(5, 'Internasional', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE IF NOT EXISTS `kegiatan` (
`id` int(11) NOT NULL,
  `kegiatan` varchar(200) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `peranan` varchar(50) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `kegiatan`, `tahun`, `peranan`, `id_kategori`, `id_dosen`) VALUES
(1, 'asad', 1231, NULL, NULL, NULL),
(4, NULL, 0, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penelitian`
--

CREATE TABLE IF NOT EXISTS `penelitian` (
`id` int(11) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `sumber_dana` varchar(50) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `id_keterangan` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `penelitian`
--

INSERT INTO `penelitian` (`id`, `judul`, `jabatan`, `tahun`, `sumber_dana`, `id_kategori`, `id_dosen`, `id_keterangan`) VALUES
(1, 'asad', 'qwqeq', 12312, 'dasad', 1, 1, 1),
(2, 'dadad', 'llll', 2000, 'llll', 1, 3, 1),
(3, 'dasd', 'fgs', 1111, 'fdsd', 1, 3, 1),
(8, '', '', 0, '', 1, 3, 1),
(9, 'adas', 'das', 2000, 'ada', 1, 3, 1),
(10, 'aaa', 'aaa', 2000, 'aaa', 1, 4, 1),
(11, 'ates', 'jkdf', 0, 'gfg', 2, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penghargaan`
--

CREATE TABLE IF NOT EXISTS `penghargaan` (
`id` int(11) NOT NULL,
  `nama_penghargaan` varchar(200) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `institusi` varchar(50) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_group_users`
--

CREATE TABLE IF NOT EXISTS `sys_group_users` (
`id` int(11) NOT NULL,
  `level` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `sys_group_users`
--

INSERT INTO `sys_group_users` (`id`, `level`, `deskripsi`) VALUES
(1, 'admin', 'Administrator'),
(2, 'Dosen', 'dosen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_menu`
--

CREATE TABLE IF NOT EXISTS `sys_menu` (
`id` int(11) NOT NULL,
  `nav_act` varchar(150) DEFAULT NULL,
  `page_name` varchar(150) DEFAULT NULL,
  `url` varchar(100) NOT NULL,
  `main_table` varchar(150) DEFAULT NULL,
  `icon` varchar(150) DEFAULT NULL,
  `urutan_menu` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `dt_table` enum('Y','N') NOT NULL,
  `tampil` enum('Y','N') NOT NULL,
  `type_menu` enum('main','page') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `sys_menu`
--

INSERT INTO `sys_menu` (`id`, `nav_act`, `page_name`, `url`, `main_table`, `icon`, `urutan_menu`, `parent`, `dt_table`, `tampil`, `type_menu`) VALUES
(1, NULL, 'master data', '', NULL, 'fa-book', 1, 0, 'Y', 'Y', 'main'),
(2, 'tabel_kategori', 'tabel kategori', 'tabel-kategori', 'tabel_kategori', 'fa-circle', 1, 1, 'Y', 'Y', 'page'),
(3, 'kategori', 'kategori', 'kategori', 'kategori', 'fa-circle', 2, 1, 'Y', 'Y', 'page'),
(4, 'jenjang_pendidikan', 'jenjang pendidikan', 'jenjang-pendidikan', 'jenjang_pendidikan', 'fa-book', 3, 1, 'Y', 'Y', 'page'),
(5, 'fakultas', 'fakultas', 'fakultas', 'fak', 'fa-book', 4, 1, 'Y', 'Y', 'page'),
(6, 'jurusan', 'jurusan', 'jurusan', 'jurusan', 'fa-book', 5, 1, 'Y', 'Y', 'page'),
(7, 'tabel_keterangan', 'tabel keterangan', 'tabel-keterangan', 'tabel_keterangan', 'fa-circle', 6, 1, 'Y', 'Y', 'page'),
(9, 'profil_dosen', 'profil dosen', 'profil-dosen', 'dosen', 'fa-book', 1, 0, 'Y', 'Y', 'page'),
(10, 'dosen', 'dosen', 'dosen', 'dosen', 'fa-circle', 1, 1, 'Y', 'Y', 'page'),
(11, 'berita', 'berita', 'berita', 'berita', 'fa-share', 1, 0, 'Y', 'Y', 'page');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_menu_role`
--

CREATE TABLE IF NOT EXISTS `sys_menu_role` (
`id` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `read_act` enum('Y','N') DEFAULT NULL,
  `insert_act` enum('Y','N') DEFAULT NULL,
  `update_act` enum('Y','N') DEFAULT NULL,
  `delete_act` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data untuk tabel `sys_menu_role`
--

INSERT INTO `sys_menu_role` (`id`, `id_menu`, `group_id`, `read_act`, `insert_act`, `update_act`, `delete_act`) VALUES
(1, 1, 1, 'Y', 'Y', 'Y', 'Y'),
(2, 2, 1, 'Y', 'Y', 'Y', 'Y'),
(3, 3, 1, 'Y', 'Y', 'Y', 'Y'),
(4, 4, 1, 'Y', 'Y', 'Y', 'Y'),
(5, 5, 1, 'Y', 'Y', 'Y', 'Y'),
(6, 6, 1, 'Y', 'Y', 'Y', 'Y'),
(7, 7, 1, 'Y', 'Y', 'Y', 'Y'),
(9, 9, 1, 'Y', 'Y', 'Y', 'Y'),
(10, 1, 2, 'N', 'N', 'N', 'N'),
(11, 2, 2, 'N', 'N', 'N', 'N'),
(12, 3, 2, 'N', 'N', 'N', 'N'),
(13, 4, 2, 'N', 'N', 'N', 'N'),
(14, 5, 2, 'N', 'N', 'N', 'N'),
(15, 6, 2, 'N', 'N', 'N', 'N'),
(16, 7, 2, 'N', 'N', 'N', 'N'),
(17, 9, 2, 'Y', 'Y', 'Y', 'Y'),
(25, 10, 1, 'Y', 'Y', 'Y', 'Y'),
(26, 10, 2, 'Y', 'N', 'N', 'N'),
(27, 11, 1, 'Y', 'Y', 'Y', 'Y'),
(28, 11, 2, 'Y', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_users`
--

CREATE TABLE IF NOT EXISTS `sys_users` (
`id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `foto_user` varchar(150) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `sys_users`
--

INSERT INTO `sys_users` (`id`, `first_name`, `last_name`, `username`, `password`, `email`, `date_created`, `foto_user`, `id_group`, `aktif`, `id_dosen`) VALUES
(1, 'mohamad ', 'wildannudin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'wildannudin@gmail.com', '2015-01-26', '10965740_10206190197982755_22114424_n.jpg', 1, 'Y', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_kategori`
--

CREATE TABLE IF NOT EXISTS `tabel_kategori` (
`id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `tabel_kategori`
--

INSERT INTO `tabel_kategori` (`id`, `nama`) VALUES
(1, 'kegiatan'),
(2, 'buku'),
(3, 'jurnal'),
(4, 'penelitian'),
(5, 'karya ilmiah'),
(6, 'pengabdian'),
(7, 'penghargaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_keterangan`
--

CREATE TABLE IF NOT EXISTS `tabel_keterangan` (
`id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tabel_keterangan`
--

INSERT INTO `tabel_keterangan` (`id`, `nama`) VALUES
(1, 'Penelitian'),
(2, 'Pengabdian');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`id`), ADD KEY `FK__sys_users` (`id_admin`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_buku_dosen` (`id_dosen`), ADD KEY `FK_buku_kategori` (`id_kategori`);

--
-- Indexes for table `detail_pendidikan`
--
ALTER TABLE `detail_pendidikan`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_detail_pendidikan_jenjang_pendidikan` (`id_jenjang`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_dosen_jurusan` (`kode_jurusan`);

--
-- Indexes for table `fak`
--
ALTER TABLE `fak`
 ADD PRIMARY KEY (`kode_fak`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_jabatan_dosen` (`id_dosen`);

--
-- Indexes for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_jenjang_pendidikan_dosen` (`id_dosen`);

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_jurnal_dosen` (`id_dosen`), ADD KEY `FK_jurnal_kategori` (`id_kategori`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
 ADD PRIMARY KEY (`kode_jurusan`), ADD KEY `kode_fak` (`kode_fak`);

--
-- Indexes for table `karya_ilmiah`
--
ALTER TABLE `karya_ilmiah`
 ADD PRIMARY KEY (`id`), ADD KEY `FK__dosen` (`id_dosen`), ADD KEY `FK_karya_ilmiah_kategori` (`id_kategori`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_kategori_tabel_kategori` (`id_tabel`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_kegiatan_dosen` (`id_dosen`), ADD KEY `FK_kegiatan_kategori` (`id_kategori`);

--
-- Indexes for table `penelitian`
--
ALTER TABLE `penelitian`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_Penelitian_dosen` (`id_dosen`), ADD KEY `FK_penelitian_kategori` (`id_kategori`), ADD KEY `FK_penelitian_tabel_keterangan` (`id_keterangan`);

--
-- Indexes for table `penghargaan`
--
ALTER TABLE `penghargaan`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_Penghargaan_dosen` (`id_dosen`), ADD KEY `FK_penghargaan_kategori` (`id_kategori`);

--
-- Indexes for table `sys_group_users`
--
ALTER TABLE `sys_group_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_menu`
--
ALTER TABLE `sys_menu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_menu_role`
--
ALTER TABLE `sys_menu_role`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_sys_menu_role_sys_menu` (`id_menu`), ADD KEY `FK_sys_menu_role_sys_users` (`group_id`);

--
-- Indexes for table `sys_users`
--
ALTER TABLE `sys_users`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_sys_users_sys_group_users` (`id_group`), ADD KEY `FK_sys_users_dosen` (`id_dosen`);

--
-- Indexes for table `tabel_kategori`
--
ALTER TABLE `tabel_kategori`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_keterangan`
--
ALTER TABLE `tabel_keterangan`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detail_pendidikan`
--
ALTER TABLE `detail_pendidikan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fak`
--
ALTER TABLE `fak`
MODIFY `kode_fak` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `karya_ilmiah`
--
ALTER TABLE `karya_ilmiah`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `penelitian`
--
ALTER TABLE `penelitian`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `penghargaan`
--
ALTER TABLE `penghargaan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_group_users`
--
ALTER TABLE `sys_group_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sys_menu`
--
ALTER TABLE `sys_menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sys_menu_role`
--
ALTER TABLE `sys_menu_role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `sys_users`
--
ALTER TABLE `sys_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tabel_kategori`
--
ALTER TABLE `tabel_kategori`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tabel_keterangan`
--
ALTER TABLE `tabel_keterangan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `berita`
--
ALTER TABLE `berita`
ADD CONSTRAINT `FK__sys_users` FOREIGN KEY (`id_admin`) REFERENCES `sys_users` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `buku`
--
ALTER TABLE `buku`
ADD CONSTRAINT `FK_buku_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_buku_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `detail_pendidikan`
--
ALTER TABLE `detail_pendidikan`
ADD CONSTRAINT `FK_detail_pendidikan_jenjang_pendidikan` FOREIGN KEY (`id_jenjang`) REFERENCES `jenjang_pendidikan` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `dosen`
--
ALTER TABLE `dosen`
ADD CONSTRAINT `FK_dosen_jurusan` FOREIGN KEY (`kode_jurusan`) REFERENCES `jurusan` (`kode_jurusan`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
ADD CONSTRAINT `FK_jabatan_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
ADD CONSTRAINT `FK_jenjang_pendidikan_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jurnal`
--
ALTER TABLE `jurnal`
ADD CONSTRAINT `FK_jurnal_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_jurnal_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
ADD CONSTRAINT `jurusan_ibfk_1` FOREIGN KEY (`kode_fak`) REFERENCES `fak` (`kode_fak`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `karya_ilmiah`
--
ALTER TABLE `karya_ilmiah`
ADD CONSTRAINT `FK__dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_karya_ilmiah_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kategori`
--
ALTER TABLE `kategori`
ADD CONSTRAINT `FK_kategori_tabel_kategori` FOREIGN KEY (`id_tabel`) REFERENCES `tabel_kategori` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
ADD CONSTRAINT `FK_kegiatan_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_kegiatan_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penelitian`
--
ALTER TABLE `penelitian`
ADD CONSTRAINT `FK_Penelitian_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_penelitian_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_penelitian_tabel_keterangan` FOREIGN KEY (`id_keterangan`) REFERENCES `tabel_keterangan` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penghargaan`
--
ALTER TABLE `penghargaan`
ADD CONSTRAINT `FK_Penghargaan_dosen` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `FK_penghargaan_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sys_menu_role`
--
ALTER TABLE `sys_menu_role`
ADD CONSTRAINT `FK_sys_menu_role_sys_group_users` FOREIGN KEY (`group_id`) REFERENCES `sys_group_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_sys_menu_role_sys_menu` FOREIGN KEY (`id_menu`) REFERENCES `sys_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
