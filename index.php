<?php 
session_start();
include "admina/inc/config.php";

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<title>Profile Dosen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<link rel="shortcut icon" href="images/favicon.ico">
    
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<link href="css/colors/" rel="stylesheet" type="text/css" id="colors" />

	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500italic,700,500,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">	
    
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if IE]><html class="ie" lang="en"> <![endif]-->
	
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/jquery.nicescroll.min.js" type="text/javascript"></script>
	<script src="js/superfish.min.js" type="text/javascript"></script>
	<script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/animate.js" type="text/javascript"></script>
	<script src="js/myscript.js" type="text/javascript"></script>
	    <script src="admina/assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="admina/assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <link href="admina/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	
</head>
<body>

<!-- PRELOADER -->
<img id="preloader" src="images/preloader.gif" alt="" />
<!-- //PRELOADER -->
<div class="preloader_hide">

	<!-- PAGE -->
	<div id="page" class="single_page">
	
		<!-- HEADER -->
		<header>
			
			<!-- MENU BLOCK -->
			<div class="menu_block">
			
				<!-- CONTAINER -->
				<div class="container clearfix">
					
					<!-- LOGO -->
					<div class="logo pull-left">
						<a href="index.html" ><span class="b1">w</span><span class="b2">h</span><span class="b3">i</span><span class="b4">t</span><span class="b5">e</span></a>
					</div><!-- //LOGO -->
					
					<!-- SEARCH FORM -->
					<div id="search-form" class="pull-right">
						<form method="get" action="#">
							<input type="text" name="Search" value="Search" onFocus="if (this.value == 'Search') this.value = '';" onBlur="if (this.value == '') this.value = 'Search';" />
						</form>
					</div><!-- SEARCH FORM -->
					
					<!-- MENU -->
					<div class="pull-right">
						<nav class="navmenu center">
							<ul>
								<li class="first scroll_btn"><a href="index.html">Home</a></li>
								<li class="scroll_btn"><a href="index.html#about">About Us</a></li>
								<li class="scroll_btn"><a href="index.html#projects">Projects</a></li>
								<li class="scroll_btn"><a href="index.html#team">Team</a></li>
								<li class="scroll_btn"><a href="index.html#news">News</a></li>
								<li class="scroll_btn last"><a href="index.html#contacts">Contacts</a></li>
								<li class="sub-menu active">
									<a href="javascript:void(0);">Pages</a>
									<ul>
										<li class="active"><a href="blog.html">Blog</a></li>
										<li><a href="blog-post.html">Blog Post</a></li>
										<li><a href="portfolio-post.html">Portfolio Single Work</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div><!-- //MENU -->
				</div><!-- //MENU BLOCK -->
			</div><!-- //CONTAINER -->
		</header><!-- //HEADER -->
		
		
		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><b>The</b> Blog</h2>
				<p>Publication of the latest news, articles, and free apps.</p>
			</div>
		</section><!-- //BREADCRUMBS -->
		
		<section id="blog">
			<!--main contents-->
			 <div class="container">
			 <!-- Main content -->
			 <section class="content-header">
                    <h1>
                        Manage Profil Dosen
                    </h1>
                       <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>profil-dosen">Profil Dosen</a></li>
                        <li class="active">Profil Dosen List</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box" id="box">
                                <div class="box-header">
                                  <h3 class="box-title">List Profil Dosen</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>   
													<th>Nama</th>
													<th>Jurusan</th>
													<th>Fakultas</th>
													
                         						<th>Keahlian</th>
                         
                        			</tr>
                                      </thead>
                                        <tbody>

                                         <?php 
      $dtb=$db->fetch_custom("select dosen.nama, jurusan.nama_jurusan, dosen.id, fak.fakultas from dosen  inner join jurusan on dosen.kode_jurusan=jurusan.kode_jurusan inner join fak on jurusan.kode_fak = fak.kode_fak");

      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td>
        <td><a style='cursor: pointer;' onclick="tampilDetail('<?=$isi->id;?>');"><?=$isi->nama;?></a></td>
		<td><?=$isi->nama_jurusan;?></a></td>
		<td><?=$isi->fakultas;?></td>
		<td>Keahlian</td>
        </tr>
        <?php
        $i++;
      }
      ?>
      
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

        <div id="tampilDetail">
        	<!--<div id="loader" style="display:none;">Loading...</div>-->	

        </div>


		</section>
	</div>
		</section><!-- //BLOG -->
	</div><!-- //PAGE -->
	
	
	  <script type="text/javascript">
      $(function () {
        $("#dtb_manual").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
	<!-- CONTACTS -->
	<section id="contacts">
	</section><!-- //CONTACTS -->
	
	<!-- FOOTER -->
	<footer>
			
		<!-- CONTAINER -->
		<div class="container">
			
			
		</div><!-- //CONTAINER -->
	</footer><!-- //FOOTER -->
	

</div>
</body>
</html>

<script type="text/javascript">
function tampilDetail(isi){
	var id_dosen = $('#id_dosen1').val();
	$('#loader').fadeIn('slow');//loading
	$(document).ready(function(){
   // alert("test");
  });
	$.ajax({
		url : 'http://localhost/profiledosen/detaildosen.php',
		type : 'post',
		data : 'id_dosen='+isi,
		success : function(msg){
			alert(msg);
			$('#box').fadeOut(100);
			$("#tampilDetail").fadeIn(100);
			$("#tampilDetail").html(msg);
		}
	});
}
function back() {
	$("#tampilDetail").html('');
	$('#box').fadeIn(100);
			$("#tampilDetail").fadeOut(100);
}

</script>