

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     DOsen
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>dosen">DOsen</a></li>
                        <li class="active">Detail DOsen</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail DOsen</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="nip" class="control-label col-lg-2">nip</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nip;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nama" class="control-label col-lg-2">nama</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nama;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="gelar" class="control-label col-lg-2">gelar</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->gelar;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="kode_jurusan" class="control-label col-lg-2">kode_jurusan</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("jurusan") as $isi) {
                  if ($data_edit->kode_jurusan==$isi->kode_jurusan) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_jurusan'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="username" class="control-label col-lg-2">username</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->username;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="password" class="control-label col-lg-2">password</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->password;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>dosen" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
