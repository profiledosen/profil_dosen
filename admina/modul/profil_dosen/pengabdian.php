<!-- Button trigger modal -->
         <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>Judul</th>
                          <th>Jabatan</th>
                          <th>Tahun</th>
                          <th>Sumber Dana</th>
                          <th>Kategori</th>
                          
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select penelitian.judul, penelitian.jabatan, penelitian.tahun, penelitian.id, penelitian.sumber_dana ,penelitian.id_kategori from penelitian  inner join kategori on penelitian.id_kategori=kategori.id where penelitian.id_dosen='$data_edit->id' and penelitian.id_keterangan = 2");
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td> 
        <td><?=$isi->judul;?></td>
        <td><?=$isi->jabatan;?></td>
        <td><?=$isi->tahun;?></td>
        <td><?=$isi->sumber_dana;?></td>
        <?php $dtb1 = $db->fetch_custom("select nama_kategori from kategori where id = '$isi->id_kategori'");
              foreach ($dtb1 as $isi1) {?>
        <td><?=$isi1->nama_kategori;?></td>
        <?php } ?>
        
        <td>
        <button type="button" class="btn btn-success btn-flat" onclick="view_form_pengabdian()" data-target="#pengabdianView"><i class="fa fa-eye"></i></button> 
        <button type="button" class="btn btn-primary btn-flat" onclick="edit_form_pengabdian()" data-target="#pengabdianEdit"><i class="fa fa-pencil"></i></i></button> 
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_dosen/pengabdian_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
 
 
                                    </table>
<button type="button" class="btn btn-primary btn-lg" onclick="tampil_form_pengabdian()"  data-target="#pengabdianModal">
  Tambah Pengabdian
</button>

<!-- Modal -->
<div class="modal fade" id="pengabdianModal" tabindex="-1" role="dialog" aria-labelledby="pengabdianModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        
        <input type="hidden" disabled="" id="id_pengabdian" value="<?=$isi->id;?>">
        <input type="hidden" disabled="" id="id_dosen" value="<?=$data_edit->id;?>">
        <input type="hidden" disabled="" id="id_keterangan" value="2">


        <h4 class="modal-title" id="pengabdianModalLabel">Pengabdian</h4>
      </div>
      <div class="modal-body">
          
                   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var id_dosen = $('#id_dosen').val();
  var id_keterangan = $('#id_keterangan');
  $(document).ready(function(){
   // alert("test");
  });
  function tampil_form_pengabdian() {
     $.ajax({
        type : 'post',
        data : 'id_dosen='+id_dosen+'&id_keterangan='+id_keterangan,
        url  : '<?= base_admin() ?>modul/profil_dosen/form_add_pengabdian.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#pengabdianModal').modal('toggle');
            $('#pengabdianModal').modal('show');
          //  $('#pengabdianModal').modal('hide');
        }

    });
  }

  function view_form_pengabdian() {
    var id_pengabdian = $('#id_pengabdian').val();
     $.ajax({
        type : 'post',
        data : 'id_pengabdian='+id_pengabdian,
        url  : '<?= base_admin() ?>modul/profil_dosen/pengabdian_view.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#pengabdianModal').modal('toggle');
            $('#pengabdianModal').modal('show');
          //  $('#pengabdianModal').modal('hide');
        }

    });
  }
  function edit_form_pengabdian() {
    var id_pengabdian = $('#id_pengabdian').val();
     $.ajax({
        type : 'post',
        data : 'id_pengabdian='+id_pengabdian,
        url  : '<?= base_admin() ?>modul/profil_dosen/pengabdian_edit.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#pengabdianModal').modal('toggle');
            $('#pengabdianModal').modal('show');
          //  $('#pengabdianModal').modal('hide');
        }

    });
  }
</script>