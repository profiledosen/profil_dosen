

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Profil Dosen
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>profil-dosen">Profil Dosen</a></li>
                        <li class="active">Detail Profil Dosen</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Profil Dosen</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="nip" class="control-label col-lg-2">nip</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nip;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nama" class="control-label col-lg-2">nama</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nama;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="gelar" class="control-label col-lg-2">gelar</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->gelar;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="kode_jurusan" class="control-label col-lg-2">kode_jurusan</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("jurusan") as $isi) {
                  if ($data_edit->kode_jurusan==$isi->kode_jurusan) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_jurusan'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                                      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#penelitian" data-toggle="tab"><i class="fa fa-book"></i>&nbsp;&nbsp;Penelitian</a></li>
              <li><a href="#pengabdian" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Pengabdian</a></li>
           
              <li><a href="#penghargaan" data-toggle="tab"><i class="fa fa-money"></i>&nbsp;&nbsp;Penghargaan</a></li>
              <li><a href="#karya_ilmiah" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Karya Ilmiah</a></li>
            <li><a href="#kegiatan" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Kegiatan</a></li>
            <li><a href="#jurnal" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Jurnal</a></li>
            <li><a href="#buku" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Buku</a></li>
            <li><a href="#jabatan" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Jabatan</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="penelitian"> 
              <?php include "penelitian.php";?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="pengabdian">
                <?php include "pengabdian.php";?>
              </div>

               <div class="tab-pane" id="penghargaan">
               <?php include "penghargaan.php";?>
              </div>
               <div class="tab-pane" id="karya_ilmiah">
                <?php include 'karya_ilmiah.php'; ?>
              </div>
              <div class="tab-pane" id="kegiatan">
                <?php include 'kegiatan.php'; ?>
              </div>
              <div class="tab-pane" id="jurnal">
                <?php include 'jurnal.php'; ?>
              </div>
              <div class="tab-pane" id="buku">
                <?php include "buku.php";?>
              </div>
              <div class="tab-pane" id="jabatan">
                <?php include "jabatan.php";?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
                  
                </section><!-- /.content -->
                    
                
                  </div>
                  </div>
              </div>
</div>
                 


                </section><!-- /.content -->
        
