
<?php
//error_reporting(0);
session_start();
include '../../inc/config.php';
include '../../inc/fungsi.php';
$id = $_POST['id_karya_ilmiah'];
$lihat=$db->fetch_single_row("karya_ilmiah","id",$id);

?>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">                
                  <div class="box-body">
                   <form class="form-horizontal">

                      <div class="form-group">
                        <label for="Judul" class="control-label col-lg-2">Judul</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$lihat->judul;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
                          
                      <div class="form-group">
                        <label for="Tahun" class="control-label col-lg-2">Tahun</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$lihat->tahun;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                      <div class="form-group">
                        <label for="jenis" class="control-label col-lg-2">Jenis</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$lihat->jenis;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                      <div class="form-group">
                        <label for="Kategori" class="control-label col-lg-2">Kategori</label>
                        <div class="col-lg-10">
                        <?php $value_kategori = $db->fetch_custom("select nama_kategori from kategori where id = '$lihat->id_kategori'");
                          foreach ($value_kategori as $nama) {?>
                          <input type="text" disabled="" value="<?=$nama->nama_kategori;?>" class="form-control">
                          <?php } ?>
                        </div>
                      </div><!-- /.form-group -->

                    </form>
                  </div>
                  </div>
              </div>
</div>