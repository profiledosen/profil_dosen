<!-- Button trigger modal -->
         <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>Nama Penghargaan</th>
                          <th>Tahun</th>
                          <th>Institusi</th>
                          <th>Kategori</th>
                          
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select penghargaan.nama_penghargaan, penghargaan.tahun, penghargaan.id, penghargaan.institusi ,penghargaan.id_kategori from penghargaan  inner join kategori on penghargaan.id_kategori=kategori.id where penghargaan.id_dosen='$data_edit->id'");
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td> 
        <td><?=$isi->nama_penghargaan;?></td>
        <td><?=$isi->tahun;?></td>
        <td><?=$isi->institusi;?></td>
        <?php $dtb1 = $db->fetch_custom("select nama_kategori from kategori where id = '$isi->id_kategori'");
              foreach ($dtb1 as $isi1) {?>
        <td><?=$isi1->nama_kategori;?></td>
        <?php } ?>
        
        <td>
        <button type="button" class="btn btn-success btn-flat" onclick="view_form_penghargaan()" data-target="#penghargaanView"><i class="fa fa-eye"></i></button> 
        <button type="button" class="btn btn-primary btn-flat" onclick="edit_form_penghargaan()" data-target="#penghargaanEdit"><i class="fa fa-pencil"></i></i></button> 
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_dosen/penghargaan_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
 
 
                                    </table>
<button type="button" class="btn btn-primary btn-lg" onclick="tampil_form_penghargaan()"  data-target="#pengabdianModal">
  Tambah Penghargaan
</button>

<!-- Modal -->
<div class="modal fade" id="penghargaanModal" tabindex="-1" role="dialog" aria-labelledby="penghargaanModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        
        <input type="hidden" disabled="" id="id_penghargaan" value="<?=$isi->id;?>">
        <input type="hidden" disabled="" id="id_dosen" value="<?=$data_edit->id;?>">
        


        <h4 class="modal-title" id="penghargaanModalLabel">Pengahargaan</h4>
      </div>
      <div class="modal-body">
          
                   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var id_dosen = $('#id_dosen').val();
  $(document).ready(function(){
   // alert("test");
  });
  function tampil_form_penghargaan() {
     $.ajax({
        type : 'post',
        data : 'id_dosen='+id_dosen,
        url  : '<?= base_admin() ?>modul/profil_dosen/form_add_penghargaan.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#penghargaanModal').modal('toggle');
            $('#penghargaanModal').modal('show');
          //  $('#penghargaanModal').modal('hide');
        }

    });
  }

  function view_form_penghargaan() {
    var id_penghargaan = $('#id_penghargaan').val();
     $.ajax({
        type : 'post',
        data : 'id_penghargaan='+id_penghargaan,
        url  : '<?= base_admin() ?>modul/profil_dosen/penghargaan_view.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#penghargaanModal').modal('toggle');
            $('#penghargaanModal').modal('show');
          //  $('#penghargaanModal').modal('hide');
        }

    });
  }
  function edit_form_penghargaan() {
    var id_penghargaan = $('#id_penghargaan').val();
     $.ajax({
        type : 'post',
        data : 'id_penghargaan='+id_penghargaan,
        url  : '<?= base_admin() ?>modul/profil_dosen/penghargaan_edit.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#penghargaanModal').modal('toggle');
            $('#penghargaanModal').modal('show');
          //  $('#penghargaanModal').modal('hide');
        }

    });
  }
</script>