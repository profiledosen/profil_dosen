
<?php
//error_reporting(0);
session_start();
include '../../inc/config.php';
include '../../inc/fungsi.php';
$id = $_POST['id_jabatan'];
$lihat=$db->fetch_single_row("jabatan","id",$id);

?>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">                
                  <div class="box-body">
                   <form class="form-horizontal">

                      <div class="form-group">
                        <label for="Jabatan" class="control-label col-lg-2">Jabatan</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$lihat->jabatan;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
                          
                      <div class="form-group">
                        <label for="Institusi" class="control-label col-lg-2">Institusi</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$lihat->institusi;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                      <div class="form-group">
                        <label for="Tahun" class="control-label col-lg-2">Tahun</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$lihat->tahun;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                    </form>
                  </div>
                  </div>
              </div>
</div>