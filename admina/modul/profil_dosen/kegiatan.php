<!-- Button trigger modal -->
         <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>Kegiatan</th>
                          <th>Tahun</th>
                          <th>Peranan</th>
                          <th>Kategori</th>
                          
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select kegiatan.kegiatan, kegiatan.tahun, kegiatan.peranan ,kegiatan.id from kegiatan  inner join kategori on kegiatan.id_kategori=kategori.id where kegiatan.id_dosen='$data_edit->id'");
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td>
        <td><?=$isi->kegiatan;?></td>
        <td><?=$isi->tahun;?></a></td>
        <td><?=$isi->peranan;?></td>
        <td><?=$isi->id;?></td>

        <td>
        <button type="button" class="btn btn-success btn-flat" onclick="view_form_kegiatan()" data-target="#kegiatanView"><i class="fa fa-eye"></i></button> 
        <button type="button" class="btn btn-primary btn-flat" onclick="edit_form_kegiatan()" data-target="#kegiatanEdit"><i class="fa fa-pencil"></i></i></button> 
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_dosen/kegiatan_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
                                    </table>
<button type="button" class="btn btn-primary btn-lg" onclick="tampil_form_kegiatan()">
  Tambah Kegiatan
</button>

<!-- Modal -->
<div class="modal fade" id="kegiatanModal" tabindex="-1" role="dialog" aria-labelledby="kegiatanModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <input type="hidden" disabled="" id="id_kegiatan" value="<?=$isi->id;?>">
        <input type="hidden" disabled="" id="id_dosen1" value="<?=$data_edit->id;?>">

        <h4 class="modal-title" id="kegiatanModalLabel">Tambah Kegiatan</h4>
      </div>
      <div class="modal-body">
      
                   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var id_dosen1 = $('#id_dosen1').val();
  $(document).ready(function(){
   // alert("test");
  });
  
  function tampil_form_kegiatan() {
     $.ajax({

        type : 'post',
        data : 'id_dosen1='+id_dosen1,
        url  : '<?= base_admin() ?>modul/profil_dosen/form_add_kegiatan.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#kegiatanModal').modal('toggle');
            $('#kegiatanModal').modal('show');
          //  $('#kegiatanModal').modal('hide');
        }

    });
  }
  
  function view_form_kegiatan() {
    var id_kegiatan = $('#id_kegiatan').val();
     $.ajax({
        type : 'post',
        data : 'id_kegiatan='+id_kegiatan,
        url  : '<?= base_admin() ?>modul/profil_dosen/kegiatan_view.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#kegiatanModal').modal('toggle');
            $('#kegiatanModal').modal('show');
          //  $('#kegiatanModal').modal('hide');
        }

    });
  }
  function edit_form_kegiatan() {
    var id_kegiatan = $('#id_kegiatan').val();
     $.ajax({
        type : 'post',
        data : 'id_kegiatan='+id_kegiatan,
        url  : '<?= base_admin() ?>modul/profil_dosen/kegiatan_edit.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#kegiatanModal').modal('toggle');
            $('#kegiatanModal').modal('show');
          //  $('#kegiatanModal').modal('hide');
        }

    });
  }
</script>