
<!-- Button trigger modal -->
         <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>Jabatan</th>
                          <th>Institusi</th>
                          <th>Tahun</th>

                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select jabatan.id, jabatan.jabatan, jabatan.institusi, jabatan.tahun  from jabatan where jabatan.id_dosen='$data_edit->id'");
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td> 
        <td><?=$isi->jabatan;?></td>
        <td><?=$isi->institusi;?></td>
        <td><?=$isi->tahun;?></td>
        
        <td>
        <button type="button" class="btn btn-success btn-flat" onclick="view_form_jabatan()" data-target="#jabatanView"><i class="fa fa-eye"></i></button> 
        <button type="button" class="btn btn-primary btn-flat" onclick="edit_form_jabatan()" data-target="#jabatanEdit"><i class="fa fa-pencil"></i></i></button> 
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_dosen/jabatan_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
 
 
                                    </table>
<button type="button" class="btn btn-primary btn-lg" onclick="tampil_form_jabatan()"  data-target="#jabatanModal">
  Tambah jabatan
</button>

<!-- Modal -->
<div class="modal fade" id="jabatanModal" tabindex="-1" role="dialog" aria-labelledby="jabatanModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        
        <input type="hidden" disabled="" id="id_jabatan" value="<?=$isi->id;?>">
        <input type="hidden" disabled="" id="id_dosen" value="<?=$data_edit->id;?>">


        <h4 class="modal-title" id="jabatanModalLabel">jabatan</h4>
      </div>
      <div class="modal-body">
          
                   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var id_dosen = $('#id_dosen').val();
  $(document).ready(function(){
   //alert("test");
  });
  function tampil_form_jabatan() {
     $.ajax({
        type : 'post',
        data : 'id_dosen='+id_dosen,
        url  : '<?= base_admin() ?>modul/profil_dosen/form_add_jabatan.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#jabatanModal').modal('toggle');
            $('#jabatanModal').modal('show');
          // $('#jabatanModal').modal('hide');
        }

    });
  }

  function view_form_jabatan() {
    var id_jabatan = $('#id_jabatan').val();
     $.ajax({
        type : 'post',
        data : 'id_jabatan='+id_jabatan,
        url  : '<?= base_admin() ?>modul/profil_dosen/jabatan_view.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#jabatanModal').modal('toggle');
            $('#jabatanModal').modal('show');
          //  $('#jabatanModal').modal('hide');
        }

    });
  }
  function edit_form_jabatan() {
    var id_jabatan = $('#id_jabatan').val();
     $.ajax({
        type : 'post',
        data : 'id_jabatan='+id_jabatan,
        url  : '<?= base_admin() ?>modul/profil_dosen/jabatan_edit.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#jabatanModal').modal('toggle');
            $('#jabatanModal').modal('show');
          //  $('#jabatanModal').modal('hide');
        }

    });
  }
</script>
