<!-- Button trigger modal -->
         <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>Judul</th>
                          <th>Nama Jurnal</th>
                          <th>Tahun</th>
                          <th>Volume</th>
                          <th>Nomor Registrasi</th>
                          <th>Kategori</th>
                          
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select jurnal.judul, jurnal.nama_jurnal, jurnal.tahun, jurnal.volume, jurnal.no_registrasi ,jurnal.id from jurnal inner join kategori on jurnal.id_kategori=kategori.id where jurnal.id_dosen='$data_edit->id'");
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td>
        <td><?=$isi->judul;?></td>
        <td><?=$isi->nama_jurnal;?></a></td>
        <td><?=$isi->tahun;?></td>
        <td><?=$isi->volume;?></td>
        <td><?=$isi->no_registrasi;?></td>
        <td><?=$isi->id;?></td>

        <td>
        <a href="<?=base_index();?>profil-dosen/detail/<?=$isi->id;?>" class="btn btn-success btn-flat"><i class="fa fa-eye"></i></a> 
        <?=($role_act["up_act"]=="Y")?'<a   onclick="edit('.$isi->id.')"  class="btn btn-primary btn-flat"><i class="fa fa-pencil"></i></a>':"";?>  
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_dosen/jurnal_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
                                    </table>
<button type="button" class="btn btn-primary btn-lg" onclick="tampil_form_jurnal()">
  Tambah Jurnal
</button>

<!-- Modal -->
<div class="modal fade" id="jurnalModal" tabindex="-1" role="dialog" aria-labelledby="jurnalModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <input type="hidden" disabled="" id="id_jurnal" value="<?=$isi->id;?>">
        <input type="hidden" disabled="" id="id_dosen1" value="<?=$data_edit->id;?>">

        <h4 class="modal-title" id="jurnalModalLabel">Tambah Jurnal</h4>
      </div>
      <div class="modal-body">
      
                   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var id_dosen1 = $('#id_dosen1').val();
  $(document).ready(function(){
   // alert("test");
  });
  
  function tampil_form_jurnal() {
     $.ajax({

        type : 'post',
        data : 'id_dosen1='+id_dosen1,
        url  : '<?= base_admin() ?>modul/profil_dosen/form_add_jurnal.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#jurnalModal').modal('toggle');
            $('#jurnalModal').modal('show');
          //  $('#jurnalModal').modal('hide');
        }

    });
  }
  
  function view_form_jurnal() {
    var id_jurnal = $('#id_jurnal').val();
     $.ajax({
        type : 'post',
        data : 'id_jurnal='+id_jurnal,
        url  : '<?= base_admin() ?>modul/profil_dosen/jurnal_view.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#jurnalModal').modal('toggle');
            $('#jurnalModal').modal('show');
          //  $('#jurnalModal').modal('hide');
        }

    });
  }
  function edit_form_jurnal() {
    var id_jurnal = $('#id_jurnal').val();
     $.ajax({
        type : 'post',
        data : 'id_jurnal='+id_jurnal,
        url  : '<?= base_admin() ?>modul/profil_dosen/jurnal_edit.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#jurnalModal').modal('toggle');
            $('#jurnalModal').modal('show');
          //  $('#jurnalModal').modal('hide');
        }

    });
  }
</script>