<!-- Button trigger modal -->

         <table id="dtb_manual" class="table table-bordered table-striped">
                                   
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>Judul</th>
                          <th>Tahun</th>
                          <th>Jenis</th>
                          <th>Kategori</th>
                          
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select karya_ilmiah.judul, karya_ilmiah.tahun, karya_ilmiah.jenis ,karya_ilmiah.id, karya_ilmiah.id_kategori from karya_ilmiah  inner join kategori on karya_ilmiah.id_kategori=kategori.id where karya_ilmiah.id_dosen='$data_edit->id'");
       
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td>
        <td><?=$isi->judul;?></td>
        <td><?=$isi->tahun;?></td>
        <td><?=$isi->jenis;?></td>
        <?php $dtb1 = $db->fetch_custom("select nama_kategori from kategori where id = '$isi->id_kategori'");
              foreach ($dtb1 as $isi1) {?>
        <td><?=$isi1->nama_kategori;?></td>
        <?php } ?>

        <td>
        <button type="button" class="btn btn-success btn-flat" onclick="view_form_karya_ilmiah()" data-target="#karya_ilmiahView"><i class="fa fa-eye"></i></button> 
        <button type="button" class="btn btn-primary btn-flat" onclick="edit_form_karya_ilmiah()" data-target="#karya_ilmiahEdit"><i class="fa fa-pencil"></i></i></button> 
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_dosen/karya_ilmiah_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
                                    </table>

<button type="button" class="btn btn-primary btn-lg" onclick="tampil_form_karya_ilmiah()">
  Tambah Karya Ilmiah
</button>

<!-- Modal -->
<div class="modal fade" id="karya_ilmiahModal" tabindex="-1" role="dialog" aria-labelledby="karya_ilmiahModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <input type="hidden" disabled="" id="id_karya_ilmiah" value="<?=$isi->id;?>">
        <input type="hidden" disabled="" id="id_dosen1" value="<?=$data_edit->id;?>">

        <h4 class="modal-title" id="karya_ilmiahModalLabel">Tambah Karya Ilmiah</h4>
      </div>
      <div class="modal-body">
      
                   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var id_dosen1 = $('#id_dosen1').val();
  $(document).ready(function(){
   // alert("test");
  });
  
  function tampil_form_karya_ilmiah() {
     $.ajax({

        type : 'post',
        data : 'id_dosen1='+id_dosen1,
        url  : '<?= base_admin() ?>modul/profil_dosen/form_add_karya_ilmiah.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#karya_ilmiahModal').modal('toggle');
            $('#karya_ilmiahModal').modal('show');
          //  $('#karya_ilmiahModal').modal('hide');
        }

    });
  }
  
  function view_form_karya_ilmiah() {
    var id_karya_ilmiah = $('#id_karya_ilmiah').val();
     $.ajax({
        type : 'post',
        data : 'id_karya_ilmiah='+id_karya_ilmiah,
        url  : '<?= base_admin() ?>modul/profil_dosen/karya_ilmiah_view.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#karya_ilmiahModal').modal('toggle');
            $('#karya_ilmiahModal').modal('show');
          //  $('#karya_ilmiahModal').modal('hide');
        }

    });
  }
  function edit_form_karya_ilmiah() {
    var id_karya_ilmiah = $('#id_karya_ilmiah').val();
     $.ajax({
        type : 'post',
        data : 'id_karya_ilmiah='+id_karya_ilmiah,
        url  : '<?= base_admin() ?>modul/profil_dosen/karya_ilmiah_edit.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#karya_ilmiahModal').modal('toggle');
            $('#karya_ilmiahModal').modal('show');
          //  $('#karya_ilmiahModal').modal('hide');
        }

    });
  }
</script>