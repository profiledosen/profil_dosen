
<!-- Button trigger modal -->
         <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>Judul Buku</th>
                          <th>Tahun</th>
                          <th>ISBN</th>
                          <th>Penerbit</th>
                          <th>Kategori</th>
                          
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select buku.id, buku.judul_buku, buku.tahun, buku.isbn, buku.penerbit, buku.id_kategori from buku inner join kategori on buku.id_kategori=kategori.id where buku.id_dosen='$data_edit->id'");
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td> 
        <td><?=$isi->judul_buku;?></td>
        <td><?=$isi->tahun;?></td>
        <td><?=$isi->isbn;?></td>
        <td><?=$isi->penerbit;?></td>
        <?php $dtb1 = $db->fetch_custom("select nama_kategori from kategori where id = '$isi->id_kategori'");
              foreach ($dtb1 as $isi1) {?>
        <td><?=$isi1->nama_kategori;?></td>
        <?php } ?>
        
        <td>
        <button type="button" class="btn btn-success btn-flat" onclick="view_form_buku()" data-target="#bukuView"><i class="fa fa-eye"></i></button> 
        <button type="button" class="btn btn-primary btn-flat" onclick="edit_form_buku()" data-target="#bukuEdit"><i class="fa fa-pencil"></i></i></button> 
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_dosen/buku_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
 
 
                                    </table>
<button type="button" class="btn btn-primary btn-lg" onclick="tampil_form_buku()"  data-target="#bukuModal">
  Tambah Buku
</button>

<!-- Modal -->
<div class="modal fade" id="bukuModal" tabindex="-1" role="dialog" aria-labelledby="bukuModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        
        <input type="hidden" disabled="" id="id_buku" value="<?=$isi->id;?>">
        <input type="hidden" disabled="" id="id_dosen" value="<?=$data_edit->id;?>">


        <h4 class="modal-title" id="bukuModalLabel">Buku</h4>
      </div>
      <div class="modal-body">
          
                   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var id_dosen = $('#id_dosen').val();
  var id_keterangan = $('#id_keterangan');
  $(document).ready(function(){
   // alert("test");
  });
  function tampil_form_buku() {
     $.ajax({
        type : 'post',
        data : 'id_dosen='+id_dosen,
        url  : '<?= base_admin() ?>modul/profil_dosen/form_add_buku.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#bukuModal').modal('toggle');
            $('#bukuModal').modal('show');
          // $('#bukuModal').modal('hide');
        }

    });
  }

  function view_form_buku() {
    var id_buku = $('#id_buku').val();
     $.ajax({
        type : 'post',
        data : 'id_buku='+id_buku,
        url  : '<?= base_admin() ?>modul/profil_dosen/buku_view.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#bukuModal').modal('toggle');
            $('#bukuModal').modal('show');
          //  $('#bukuModal').modal('hide');
        }

    });
  }
  function edit_form_buku() {
    var id_buku = $('#id_buku').val();
     $.ajax({
        type : 'post',
        data : 'id_buku='+id_buku,
        url  : '<?= base_admin() ?>modul/profil_dosen/buku_edit.php',
        success : function(msg) {
           $(".modal-body").html(msg);
           $('#bukuModal').modal('toggle');
            $('#bukuModal').modal('show');
          //  $('#bukuModal').modal('hide');
        }

    });
  }
</script>