<?php 
session_start();
include "admina/inc/config.php";
$id_dosen = $_POST['id_dosen'];

 ?>

            

                <!-- Main content -->
                <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Profil Dosen</h3>
                                </div>
<?php 
    $dtb=$db->fetch_custom("select dosen.id, dosen.nama, dosen.nip, dosen. gelar, dosen.kode_jurusan from dosen where dosen.id = '$id_dosen'");
 ?>
                  <div class="container">
                    <div class="table-responsive">          
                      <table class="table">
                          <tbody>
                            <tr>
                              <td>Nama</td>
                              <?php foreach ($dtb as $value) {
                                
                              ?>
                              <td><?=$value->nama;?></td>
                             
                            </tr>
                            <tr>
                              <td>NIP</td>
                              
                              <td><?=$value->nip;?></td>
                              
                            </tr>
                            <tr>
                              <td>Gelar</td>
                             
                              <td><?=$value->gelar;?></td>
                              
                            </tr>
                             <?php } ?>
                            <tr>
                              <td>Jurusan</td>
                              <?php 
                                $sql1 = $db->fetch_custom("select nama_jurusan from jurusan where kode_jurusan = '$value->kode_jurusan'");
                                foreach ($sql1 as $value1) {
                               ?>
                              
                              <td><?=$value1->nama_jurusan;?></td>
                              <?php } ?>
                            </tr>
                           
                            <tr>
                              <td>Riwayat Pendidikan</td>
                              <?php 
                                $sql2 =$db->fetch_custom("select jenjang_pendidikan.id, jenjang_pendidikan.nama_jenjang, detail_pendidikan.nama_instansi, detail_pendidikan.thn_masuk, detail_pendidikan.thn_keluar, detail_pendidikan.program_studi from jenjang_pendidikan inner join detail_pendidikan on jenjang_pendidikan.id = detail_pendidikan.id_jenjang where id_dosen = '$value->id'");
                               ?>
                              <td>
                                <?php 
                                  foreach ($sql2 as $value2) {
                                    echo $value2->nama_jenjang;?> : <?php echo $value2->nama_instansi;?>, <?php echo $value2->thn_masuk;?> - <?php echo $value2->thn_keluar;?> <br>
                                    Bidang Ilmu : <?php echo $value2->program_studi;?>
                                    <br><br> 
                                    <?php } ?>                             
                              </td>
                              
                            </tr>
                          </tbody>
                          
                      </table>
                    </div>
                                      

                  </div>
                  






          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#penelitian" data-toggle="tab"><i class="fa fa-book"></i>&nbsp;&nbsp;Penelitian</a></li>
              <li><a href="#pengabdian" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Pengabdian</a></li>
           
              <li><a href="#penghargaan" data-toggle="tab"><i class="fa fa-money"></i>&nbsp;&nbsp;Penghargaan</a></li>
              <li><a href="#karya_ilmiah" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Karya Ilmiah</a></li>
            <li><a href="#kegiatan" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Kegiatan</a></li>
            <li><a href="#jurnal" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Jurnal</a></li>
            <li><a href="#buku" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Buku</a></li>
            <li><a href="#jabatan" data-toggle="tab"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Jabatan</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="penelitian"> 
              <?php include "detail/penelitian.php";?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="pengabdian">
                <?php include "detail/pengabdian.php";?>
              </div>

               <div class="tab-pane" id="penghargaan">
               <?php include "detail/penghargaan.php";?>
              </div>
               <div class="tab-pane" id="karya_ilmiah">
                <?php include 'detail/karya_ilmiah.php'; ?>
              </div>
              <div class="tab-pane" id="kegiatan">
                <?php include 'detail/kegiatan.php'; ?>
              </div>
              <div class="tab-pane" id="jurnal">
                <?php include 'detail/jurnal.php'; ?>
              </div>
              <div class="tab-pane" id="buku">
                <?php include "detail/buku.php";?>
              </div>
              <div class="tab-pane" id="jabatan">
                <?php include "detail/jabatan.php";?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
                  
                </section><!-- /.content -->
                    
                
                  </div>
                  </div>
              </div>
              <button onclick="back()">Back</button>
</div>
                 


                </section><!-- /.content -->
        
